ALFRESCO_DIR=/opt/alfresco-4.2.f
JAVA=$ALFRESCO_DIR/java/bin/java
BACKUP_DIR=/opt/backup
ARTIFACTORY_USER=citeck
ARTIFACTORY_PASS=Qq123456
ARTIFACTORY_SERVER=http://ec2-54-246-72-179.eu-west-1.compute.amazonaws.com
ARTIFACT_DIR=dist/idocs/cardif
PATH_TO_ARTIFACTS=$ARTIFACTORY_SERVER/$ARTIFACT_DIR

CATALINA_HOME=$ALFRESCO_DIR/tomcat
ENABLE_BACKUP=false
DB_NAME=alfresco
DB_USER_NAME=postgres
DB_USER_PASSWORD=
DB_HOST=localhost
DB_PORT=5432

PROXY_SERVER=http://proxy-server:port
PROXY_USER=user
PROXY_PASS=pass
PROXY_OPTS=--proxy-negotiate

UPDATE_REPO=true
UPDATE_SHARE=true

IDOCS_CORE_VERSION=2.9.0-SNAPSHOT
IDOCS_CORE_CLASSIFIER=alfresco4.2
IDOCS_ATTORNEYS_VERSION=1.0.0-SNAPSHOT
IDOCS_ATTORNEYS_CLASSIFIER=common
IDOCS_CONTRACTS_VERSION=1.0.0-SNAPSHOT
IDOCS_CONTRACTS_CLASSIFIER=common
IDOCS_LETTERS_VERSION=1.0.0-SNAPSHOT
IDOCS_LETTERS_CLASSIFIER=common
IDOCS_ORDERS_VERSION=1.0.0-SNAPSHOT
IDOCS_ORDERS_CLASSIFIER=common
IDOCS_CUSTOM_VERSION=1.0.0-SNAPSHOT
IDOCS_CUSTOM_CLASSIFIER=common
IDOCS_CUSTOM_NAME=ecos-cardif


AMPS="deps/1st-override-repo/${IDOCS_CORE_VERSION}/1st-override-repo-${IDOCS_CORE_VERSION}-${IDOCS_CORE_CLASSIFIER}.amp
deps/idocs-repo/${IDOCS_CORE_VERSION}/idocs-repo-${IDOCS_CORE_VERSION}-${IDOCS_CORE_CLASSIFIER}.amp
deps/attorneys-repo/${IDOCS_ATTORNEYS_VERSION}/attorneys-repo-${IDOCS_ATTORNEYS_VERSION}-${IDOCS_ATTORNEYS_CLASSIFIER}.amp
deps/contracts-repo/${IDOCS_CONTRACTS_VERSION}/contracts-repo-${IDOCS_CONTRACTS_VERSION}-${IDOCS_CONTRACTS_CLASSIFIER}.amp
deps/letters-repo/${IDOCS_LETTERS_VERSION}/letters-repo-${IDOCS_LETTERS_VERSION}-${IDOCS_LETTERS_CLASSIFIER}.amp
deps/orders-repo/${IDOCS_ORDERS_VERSION}/orders-repo-${IDOCS_ORDERS_VERSION}-${IDOCS_ORDERS_CLASSIFIER}.amp
amps/${IDOCS_CUSTOM_NAME}-repo/${IDOCS_CUSTOM_VERSION}/${IDOCS_CUSTOM_NAME}-repo-${IDOCS_CUSTOM_VERSION}-${IDOCS_CUSTOM_CLASSIFIER}.amp"
AMPS_SHARE="deps_share/1st-override-share/${IDOCS_CORE_VERSION}/1st-override-share-${IDOCS_CORE_VERSION}-${IDOCS_CORE_CLASSIFIER}.amp
deps_share/idocs-share/${IDOCS_CORE_VERSION}/idocs-share-${IDOCS_CORE_VERSION}-${IDOCS_CORE_CLASSIFIER}.amp
deps_share/attorneys-share/${IDOCS_ATTORNEYS_VERSION}/attorneys-share-${IDOCS_ATTORNEYS_VERSION}-${IDOCS_ATTORNEYS_CLASSIFIER}.amp
deps_share/contracts-share/${IDOCS_CONTRACTS_VERSION}/contracts-share-${IDOCS_CONTRACTS_VERSION}-${IDOCS_CONTRACTS_CLASSIFIER}.amp
deps_share/letters-share/${IDOCS_LETTERS_VERSION}/letters-share-${IDOCS_LETTERS_VERSION}-${IDOCS_LETTERS_CLASSIFIER}.amp
deps_share/orders-share/${IDOCS_ORDERS_VERSION}/orders-share-${IDOCS_ORDERS_VERSION}-${IDOCS_ORDERS_CLASSIFIER}.amp
amps_share/${IDOCS_CUSTOM_NAME}-share/${IDOCS_CUSTOM_VERSION}/${IDOCS_CUSTOM_NAME}-share-${IDOCS_CUSTOM_VERSION}-${IDOCS_CUSTOM_CLASSIFIER}.amp"

WGET="/usr/bin/wget --user=$ARTIFACTORY_USER --password=$ARTIFACTORY_PASS -O "
CURL_PROXY="/usr/bin/curl --proxy $PROXY_SERVER --proxy-user $PROXY_USER:$PROXY_PASS $PROXY_OPTS --user $ARTIFACTORY_USER:$ARTIFACTORY_PASS -o "
CURL="/usr/bin/curl --user $ARTIFACTORY_USER:$ARTIFACTORY_PASS -o "

DOWNLOAD=$WGET

